import React, { useContext, useState } from 'react'
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import * as IoIcons from 'react-icons/io';
import * as ImIcons from 'react-icons/im';
import Link from 'next/link';
import UserContext from '../UserContext';
import { IconContext } from 'react-icons';

export default function NavBar(){ 
    const { user, setUser } = useContext(UserContext)   
    const [sidebar, setSidebar] = useState(false);

    const showSideBar = () => {
        setSidebar(!sidebar)
    }    

    return(
        <React.Fragment>
          <IconContext.Provider value={{color: '#fff'}}>
            <div className="navbar">
                <Link href="#" >
                    <a className="menu-bar"><FaIcons.FaBars onClick={showSideBar}/></a>
                </Link>
            </div>
            <nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
                <ul className="nav-menu-items"  onClick={showSideBar}>
                    <li className="navbar-toggle">
                        <Link href="#" >
                            <a className='menu-bar'><AiIcons.AiOutlineClose /></a>
                        </Link>
                    </li>
                    <li className="nav-text">
                        <Link href="/">
                            <a><AiIcons.AiFillHome /><span>Home</span></a>
                        </Link>
                    </li>
                    {(user.id !== null)
                    ?
                        (user.id) 
                        ?
                        <React.Fragment>
                            
                            <li className="nav-text">
                            <Link href="/history">
                                <a><IoIcons.IoIosPaper /><span>History</span></a>
                            </Link>
                            </li>
                            <li className="nav-text">
                                <Link href="/analytics">
                                    <a><IoIcons.IoMdAnalytics /><span>Analytics</span></a>
                                </Link>
                            </li>
                            <li className="nav-text">
                                <Link href="/profile">
                                    <a><ImIcons.ImProfile /><span>Profile</span></a>
                                </Link>
                            </li>
                            <li className="nav-text">
                                <Link href="/logout">
                                    <a><AiIcons.AiOutlineLogout /><span>Logout</span></a>
                                </Link>
                            </li>
                        </React.Fragment>
                        :
                        <React.Fragment>
                        <li className="nav-text">
                                <Link href="/login">
                                    <a><AiIcons.AiOutlineLogin/><span>Login</span></a>
                                </Link>
                            </li> 
                            <li className="nav-text">
                                <Link href="/register">
                                    <a><IoIcons.IoIosCreate /><span>Create Account</span></a>
                                </Link>
                            </li>
                        </React.Fragment>
                    :
                    <React.Fragment>
                        <li className="nav-text">
                            <Link href="/login">
                                <a><AiIcons.AiOutlineLogin/><span>Login</span></a>
                            </Link>
                        </li> 
                        <li className="nav-text">
                            <Link href="/register">
                                <a><IoIcons.IoIosCreate /><span>Create Account</span></a>
                            </Link>
                        </li>
                    </React.Fragment>
                    }                    
                </ul>
            </nav>
            </IconContext.Provider>
        </React.Fragment>
    )
}