import React, { useState, useEffect } from 'react';
import { Form, Button, Card, Col, Row } from 'react-bootstrap';
import * as AiIcons from 'react-icons/ai';
import Swal from 'sweetalert2';
import AppHelper from '../app-helper';
import Router from 'next/router';
import Link from 'next/link';

export default function RegistrationForm(){

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [balance, setBalance] = useState(0);
    const [passType, setPassType] = useState('password');

    function passwordType(){
        if(passType === 'text'){
            setPassType('password')
        console.log(passType);
        }else{
            setPassType('text')
        console.log(passType);
        }
    }

    function register(e){
        e.preventDefault()
        console.log(email)
        let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(email)) {
            // This is a gmail id.
           console.log("email is gmail");
           if((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNo.length === 11)){
               
               const options = {
                    method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email
                })
            }

            fetch(`${AppHelper.API_URL}/users/email-exists`, options)
            .then(AppHelper.toJSON)
            .then(data => {
                emailDoesNotExist(data)
            })

           }
           else{
            Swal.fire('Check All Your Credentials', 'Please check all your credentials', 'error')
           }
        }else{
           Swal.fire('Email is not a gmail', 'Email does not exist', 'error')
        }
          
    }

    function emailDoesNotExist(data){

        if( data === false ){
            console.log("data is false");
            const options = {

                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    firstName: firstName,
                    lastName: lastName,
                    email: email,
                    mobileNo: mobileNo,
                    password: password1,
                    balance: balance                                                
                })   
            }
            fetch(`${AppHelper.API_URL}/users`, options)
            .then(AppHelper.toJSON)
            .then(data => {
                registered(data)
            })
        }
        else{
            Swal.fire('Email already exists', 'Please add new email', 'error')
        }
    }
   
    function registered(data){
        if( data === true ){
            Swal.fire('Registration Successful', 'You may process to login', 'success')
            Router.push("/login") ;
        }
    }

  return(
    <React.Fragment>
        <Card.Header className="bg-dark text-white text-center">
            Register Now And Track Your Budget!!
        </Card.Header>
            <div className="bg-white p-4">
                <Form onSubmit = {(e) => register(e)}>
                    <Row>
                        <Col xs="6">
                            <Form.Group controlId ="firstName">
                                <Form.Label>First Name:</Form.Label>
                                <Form.Control  type="text" placeholder="Enter First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
                            </Form.Group>

                        </Col>
                        
                        <Col xs="6">
                            <Form.Group controlId ="lastName">
                                <Form.Label>Last Name:</Form.Label>
                                <Form.Control  type="text" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)} required/>
                            </Form.Group>
                        </Col>
                    </Row>

                    <Row>
                        <Col xs="12">
                            <Form.Group controlId ="userEmail">
                                <Form.Label>Email:</Form.Label>
                                <Form.Control  type="email" placeholder="Enter Email" value={email} onChange={e => setEmail(e.target.value)} required/>
                            </Form.Group>
                        </Col>
                    </Row>

                    <Row>
                        <Col xs="6">
                            <Form.Group controlId ="password1">
                                <Form.Label>Password:</Form.Label>
                                <Form.Control  type={passType} placeholder="Enter Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
                                <span className="passType2" onClick={passwordType}><a>{passType === 'text' ? <AiIcons.AiFillEyeInvisible/> : <AiIcons.AiFillEye/>}</a></span>
                            </Form.Group>
                        </Col>
                        <Col xs="6">
                            <Form.Group controlId ="password2">
                                <Form.Label>Re-Password:</Form.Label>
                                <Form.Control  type={passType} placeholder="Re-Type Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
                                <span className="passType2" onClick={passwordType}><a>{passType === 'text' ? <AiIcons.AiFillEyeInvisible/> : <AiIcons.AiFillEye/>}</a></span>
                            </Form.Group>
                        </Col>
                    </Row>

                    <Row>
                        <Col xs="6">
                            <Form.Group controlId ="mobileNo">
                                <Form.Label>Mobile Number:</Form.Label>
                                <Form.Control  type="number" placeholder="Enter Mobile Number" value={mobileNo} onChange={e => setMobileNo(e.target.value)} required/>
                            </Form.Group>
                        </Col>
                        <Col xs="6">
                            <Form.Group controlId ="balance">
                                <Form.Label>Enter Balance:</Form.Label>
                                <Form.Control  type="number" placeholder="Enter Balance Amount" value={balance} onChange={e => setBalance(e.target.value)} required/>
                            </Form.Group>
                        </Col>
                    </Row>
                    <Button variant="primary" type="submit" block>Submit</Button>
                </Form>
                <p>Already a member? <Link href="/login">Login Now!!</Link></p>            
            </div>            
    </React.Fragment>
  )  
}