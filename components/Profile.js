import React, { useEffect, useState} from 'react'
import AppHelper from '../app-helper';
import { Image } from 'react-bootstrap';

export default function Profile({profile}){
	//console.log(profile.doyName)

	const url = 'https://backend-buddyget.herokuapp.com/static/' + profile.doyName

 return(
		<React.Fragment>
			<Image src={url} width={150} fluid className="m-4"/>
			<h3>{profile.firstName} {profile.lastName}</h3>
			<p>Email: {profile.email}</p>
			<p>Phone: {profile.mobileNo}</p>
      		<em>Balance: PHP {profile.balance}</em>
		</React.Fragment>
	)
}
   