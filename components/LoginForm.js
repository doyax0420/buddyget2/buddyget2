import { useState, useContext } from 'react';
import { Form, Button, Card } from 'react-bootstrap';
import Router from 'next/router';
import { GoogleLogin } from 'react-google-login';
import UserContext from '../UserContext';
import AppHelper from '../app-helper';
import Swal from 'sweetalert2';
import Link from 'next/link';
import * as AiIcons from 'react-icons/ai';
  
export default function LoginForm() {

	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [passType, setPassType] = useState('password');
	
	//consume the UserContext object to get the values that are passed by it
	const { user, setUser } = useContext(UserContext)

    function passwordType(){
        if(passType === 'text'){
            setPassType('password')
        console.log(passType);
        }else{
            setPassType('text')
        console.log(passType);
        }
    }
	
	function authenticate(e){
		e.preventDefault()
		
		const options = {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}
		
		fetch(`${AppHelper.API_URL}/users/login`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)
			
			if(typeof data.access !== 'undefined'){
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)
			}else{
				if(data.error === 'does-not-exist'){
					Swal.fire('Authentication failed', 'User does not exist', 'error')
				}else if(data.error === 'incorrect-password'){
					Swal.fire('Authentication failed', 'Password is incorrect', 'error')
				}else if(data.error === 'login-type-error'){
					Swal.fire('Login Type Error', 'You may have registered through a different login method, try alternative login procedures.', 'error')
				}
			}
		})
	}

	function authenticateGoogleToken(response){
		//console.log(response)
		
		const payload = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json'},
			body: JSON.stringify({
				tokenId: response.tokenId
			})
		}

		fetch(`${AppHelper.API_URL}/users/verify-google-id-token`, payload)
		.then(AppHelper.toJSON)
		.then(data => {
			if(typeof data.access !== 'undefined'){
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)
			}else{
				if(data.error === 'google-auth-error'){
					Swal.fire('Google Auth Error', 'Google authentication procedure failed', 'error')
				}else if (data.error === 'login-type-error'){
					Swal.fire('Login Type Error', 'You may have registered through a different login method', 'error')
				}
			}
		})
	}
 
	function retrieveUserDetails(accessToken){
		const options = {
			headers: { Authorization: `Bearer ${accessToken}`}
		}
		fetch(`${AppHelper.API_URL}/users/details`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
			localStorage.setItem('personId', data._id);

			Router.push('/')
		})
	}

	return(
        <Card>
			<Card.Header className="bg-dark text-white text-center">
				Your Money Budgeting App Buddy 
			</Card.Header>
			<Card.Body>
				<Form onSubmit={e => authenticate(e)}>
					<Form.Group controlId ="userEmail">
						<Form.Label>Email Address</Form.Label>
						<Form.Control  type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId ="password">
						<Form.Label>Password</Form.Label>
                        <Form.Control  type={passType} placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} required/>
                        <span className="passType" onClick={passwordType}><a>{passType == 'text' ? <AiIcons.AiFillEyeInvisible/> : <AiIcons.AiFillEye/>}</a></span>
					</Form.Group>

					<Button variant="primary" type="submit" block>Submit</Button>
					<GoogleLogin 
					clientId="650812600787-m0fv3udd1kgu2kvd13b61rac39ootcrd.apps.googleusercontent.com"
					buttonText="Login"
					onSuccess={ authenticateGoogleToken }
					onFailure={ authenticateGoogleToken }
					cookiePolicy={ 'single_host_origin' }
					className="w-100 text-center d-flex justify-content-center" 
					/>
				</Form>
				<span> Not yet login? <Link href="/register"><a>Register Here!!</a></Link></span>
			</Card.Body>
		</Card>	
	)
}