import React, { useState, useEffect, useContext } from 'react'
import { Form, Dropdown, Button, Col, Row, Modal } from 'react-bootstrap';
import AppHelper from '../app-helper';
import UserContext from '../UserContext';
import Router from 'next/router';
import Profile from './Profile'
import moment from 'moment'

export default function Transaction(){

	const [transactionName, setTransactionName] = useState('');
	const [amountTransaction, setAmountTransaction] = useState(0);
	const [transactionType, setTransactionType] = useState('')
	const [show, setShow] = useState(false);
	const [paymentCategory, setPaymentCategory] = useState('');
	const [bal, setBal] = useState(0)
	const [categoryName, setCategoryName] = useState('')
	const [category, setCategory] = useState([])
	const [profile, setProfile] = useState([]);	

	const { user } = useContext(UserContext)
	
	function checkMenu(e){
	      //console.log(e.target.id);
	      setPaymentCategory('')
	      setTransactionType(e.target.id)

	}

	function checkCategory(e){
		setPaymentCategory(e.target.id)
	}

    const handleClose = () => setShow(false);

    const handleShow = () => setShow(true);
   
	const handleAddCategory = (e) => {
		
		e.preventDefault()

			const options = {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					categoryName: categoryName,
					transactionType: transactionType
				})
			}
			
			fetch(`${AppHelper.API_URL}/users/addCategory`, options)
			.then(res => res.json())
			.then(data => {
				if(data === true){
					setShow(false)
					const options = {
					  headers: {
					    'Authorization': `Bearer ${localStorage.getItem('token')}`,
					    'Content-Type': 'application/json'
					  }
					}

					fetch(`${AppHelper.API_URL}/users/details`, options)
					.then(res => res.json())
					.then(data => {
						setBal(data.balance)
						setCategory(data.category)
						setCategoryName('')
						setTransactionType('Transaction Type')
					})

				}
			})		
	}

	useEffect(() => {
		const options = {
		  headers: {
		    'Authorization': `Bearer ${localStorage.getItem('token')}`,
		    'Content-Type': 'application/json'
		  }
		}

		fetch(`${AppHelper.API_URL}/users/details`, options)
		.then(res => res.json())
		.then(data => {
			setBal(data.balance)
			setCategory(data.category)
		})
	}, [])

	function transactNow(e){
		e.preventDefault();

		let balanceAmount = 0;

		if( transactionType === 'income' ){

			balanceAmount = parseInt(bal) + parseInt(amountTransaction)

		}else{

			balanceAmount = parseInt(bal) - parseInt(amountTransaction)

		}
			console.log(paymentCategory)
			const options = {
			  method: 'POST',
			  headers: {
			    'Authorization': `Bearer ${localStorage.getItem('token')}`,
			    'Content-Type': 'application/json'
			  },
			  body: JSON.stringify({
			  	personId: user.id,
			  	transactionType: transactionType,
			  	categoryName: paymentCategory,
			  	amountTransaction: amountTransaction,
			  	balanceAmount: balanceAmount,
			  	description: transactionName
			  })
			}

			fetch(`${AppHelper.API_URL}/transactions/addTransactions`, options)
			.then(res => res.json())
			.then(data => {
				setTransactionName('')
				setAmountTransaction(0)

				if(data === true){

					const options = {
						method: 'PUT',
						headers: {
							'Authorization': `Bearer ${localStorage.getItem('token')}`,
							'Content-Type': 'application/json'
						},
						body: JSON.stringify({
							id: user.id,
							balance: balanceAmount
						})
					}

					fetch(`${AppHelper.API_URL}/users/updateBalance`, options)
					.then(res => res.json())
					.then(data => {
						
						const options = {
						  headers: {
						    'Content-Type': 'application/json'
						  }
						}

						fetch(`${AppHelper.API_URL}/transactions/details`, options)
						.then(res => res.json())
						.then(data => {
						  
						  	setTransactions(
			  				 	 data.map((element, key) => {
			  				 		if(element.personId === user.id){
			  				 			return(		
			  		 						{
			  		 							key: key,
			  		 							_id: element._id,
			  		 							amountTransaction: element.amountTransaction,
			  		 							balanceAmount: element.balanceAmount,
			  		 							categoryName: element.categoryName,
			  		 							dateInput: element.dateInput,
			  		 							description: element.description,
			  		 							personId: element.personId,
			  		 							transactionType: element.transactionType
			  		 						}
			  				 			)
			  				 		}	
			  				 	 })
			  				)			  

						  	const options = {
						  	  headers: {
						  	    'Authorization': `Bearer ${localStorage.getItem('token')}`,
						  	    'Content-Type': 'application/json'
						  	  }
						  	}

						  	fetch(`${AppHelper.API_URL}/users/details`, options)
						  	.then(res => res.json())
						  	.then(data => {
						  	  console.log(data)
						  	  setProfile(data)
						  	})

						})		

					})
				}
			})
	}
	let temp = [];

	if(category.length > 0){

			temp = category.map(element => {

				if(element.transactionType === 'payment'){
				
					return <Dropdown.Item id={element.categoryName}>{element.categoryName}</Dropdown.Item>

				}

			})
			
	}
	
	let temp2 = [];

	if(category.length > 0){

			temp2 = category.map(element => {

				if(element.transactionType === 'income'){
				
				return <Dropdown.Item id={element.categoryName}>{element.categoryName}</Dropdown.Item>

				}

			})
			
	}

	useEffect(() => {

	  const options = {
	    headers: {
	      'Authorization': `Bearer ${localStorage.getItem('token')}`,
	      'Content-Type': 'application/json'
	    }
	  }

	  fetch(`${AppHelper.API_URL}/users/details`, options)
	  .then(res => res.json())
	  .then(data => {
	    setProfile(data)
	  })
	}, [])

	const [transactions, setTransactions] = useState([])
	const [personId, setPersonId] = useState('');
	useEffect(() => {

		const options = {
		  headers: {
		    'Authorization': `Bearer ${localStorage.getItem('token')}`,
		    'Content-Type': 'application/json'
		  }
		}

		fetch(`${AppHelper.API_URL}/transactions/details`, options)
		.then(res => res.json())
		.then(data => {
	 
			setTransactions(
			 	 data.map((element, key) => {
			 		if(element.personId === user.id){
			 			return(		
	 						{	
	 							key: key,
	 							_id: element._id,
	 							amountTransaction: element.amountTransaction,
	 							balanceAmount: element.balanceAmount,
	 							categoryName: element.categoryName,
	 							dateInput: element.dateInput,
	 							description: element.description,
	 							personId: element.personId,
	 							transactionType: element.transactionType
	 						}
			 			)
			 		}	
			 	 })
			)	
		})

	},[])

	let trans = transactions.map(element => {
		if(element === undefined){
			console.log(element)
		}else{
			return(		
				<React.Fragment>
					<div className="trans1 bg-secondary text-white m-2 p-4">
						<p className="myDate">Date: {moment(element.dateInput).format('MMMM DD YYYY')}</p>
						{element.transactionType === 'income' ? <p className="text-success amount">Amount: +{element.amountTransaction} PHP</p> : <p className="text-danger amount">Amount: -{element.amountTransaction} PHP</p>} 
						<p className="text-warning">Description: {element.description}</p>
						<p className="text-warning">Balance: {element.balanceAmount}</p>
					</div>
				</React.Fragment>
			)
		}
	})

	return(
	<React.Fragment>
			<Col lg={2}>
			</Col>
			<Col className="col1" lg={4} md={12}>
				<div className="profile m-4 p-3 pt-5">
					<Profile profile = { profile } />
				</div>
							<div className="homeLeftBottom p-3">
								<p className="text-center">NEW TRANSACTION</p>
								<Form onSubmit={(e) => transactNow(e)}>
									<Form.Group className="text-transact" controlId ="transactionName">
										<Form.Label>What is this for?</Form.Label>
										<Form.Control  type="text" placeholder="Enter you transaction" value={transactionName} onChange={e => setTransactionName(e.target.value)} required/>
									</Form.Group>

									<Form.Group className="text-transact" controlId ="amountTransaction">
										<Form.Label>Transaction fee</Form.Label>
										<Form.Control  type="number" placeholder="Transaction amount" value={amountTransaction} onChange={e => setAmountTransaction(e.target.value)} required/>
									</Form.Group>

									<Dropdown className="dropdownMenus" onClick={(e) => checkMenu(e)}>
									  <Dropdown.Toggle variant="secondary btn-block">
									    {transactionType ? transactionType : 'Transaction Type'}
									  </Dropdown.Toggle>

									  <Dropdown.Menu>
									    <Dropdown.Item id="income">Income</Dropdown.Item>
									    <Dropdown.Item id="payment">Payment</Dropdown.Item>
									  </Dropdown.Menu>
									</Dropdown>

									<Row>
										<Col md={6}>
										  <Dropdown className="dropdownMenus" onClick={e => checkCategory(e)}>
										  <Dropdown.Toggle variant="secondary btn-block">
										    {paymentCategory ? paymentCategory : 'Category'}
										  </Dropdown.Toggle>
										  <Dropdown.Menu>
										    {transactionType === 'payment'
										    	?
										    		temp
										    	:
										    		temp2
										    }
										  </Dropdown.Menu>
										</Dropdown>
										</Col>
										<Col md={6}>
											<Button className="buttonMenu" variant="secondary" onClick={handleShow}>
										        Add Category
										    </Button>
										</Col>
									</Row>
						            <Button variant="secondary" type="submit" block>Post Transaction</Button>
								</Form>
							</div>

							<Modal show={show} onHide={handleClose}>
						        <Modal.Header closeButton>
						          <Modal.Title>Add Category</Modal.Title>
						        </Modal.Header>
						        <Modal.Body>
						        	<Form onSubmit={(e) => handleAddCategory(e)}>
						        		<Form.Group className="text-transact" controlId ="transact">
						        			<Form.Label>Category Name:</Form.Label>
						        			<Form.Control  type="text" placeholder="Enter your category name" value={categoryName} onChange={e => setCategoryName(e.target.value)} required/>
						        		</Form.Group>

						        		<Dropdown className="dropdownMenus" onClick={e => checkMenu(e)}>
						        		  <Dropdown.Toggle variant="secondary btn-block">
						        		    {transactionType ? transactionType : 'Transaction Type'}
						        		  </Dropdown.Toggle>

						        		  <Dropdown.Menu>
						        		    <Dropdown.Item id="income">Income</Dropdown.Item>
						        		    <Dropdown.Item id="payment">Payment</Dropdown.Item>
						        		  </Dropdown.Menu>
						        		</Dropdown>

						        	</Form>
						        </Modal.Body>
						        <Modal.Footer>
						          <Button variant="secondary" onClick={handleClose}>
						            Close
						          </Button>
						          <Button variant="primary" onClick={handleAddCategory}>
						            Save Changes
						          </Button>
						        </Modal.Footer>
						      </Modal>
			</Col>	
			<Col lg={5} md={12}>
	        <div className="incomeHistory">
	          <div>
	          	{trans.reverse()}
	          </div>
	        </div>
			</Col>
			<Col lg={1}>				
			</Col>				
	</React.Fragment>
	)
}