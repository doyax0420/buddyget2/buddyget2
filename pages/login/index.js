import { Row, Col } from 'react-bootstrap';
import View from '../../components/View';
import LoginForm from '../../components/LoginForm';

export default function index() {
return(
	<View title={'Welcome to BUDDYGET'}>
		<Row className="justify-content-center">
			<Col xs md="6">
				<h3 className="bg-primary text-white text-center m-3">Welcome to BUDDYGET</h3>
				<LoginForm/>
			</Col>
		</Row>
	</View>
)
}