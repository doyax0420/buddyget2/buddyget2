import { Row, Col } from 'react-bootstrap';
import View from '../../components/View';
import RegistrationForm from '../../components/RegistrationForm';

export default function index() {
return(
	<View title={'Let Us Start Your Budget'}>
		<Row className="justify-content-center">
			<Col xs md="6">
				<h3 className="bg-primary text-white text-center m-3 p-3">Let Us Start Budgeting</h3>
				<RegistrationForm/>
			</Col>
		</Row>
	</View>
)
}