import React, { useState, useEffect } from 'react'
import AppHelper from '../../app-helper';
import { Button, Container, Row, Col, Modal, Form, Image } from 'react-bootstrap';

export default function Profile(){
	const [data, setData] = useState([])
	const [show, setShow] = useState(false);
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');

	useEffect(() => {

		const options = {
		  headers: {
		    'Authorization': `Bearer ${localStorage.getItem('token')}`,
		    'Content-Type': 'application/json'
		  }
		}

		fetch(`${AppHelper.API_URL}/users/details`, options)
		.then(res => res.json())
		.then(data => {
			setData(data)
		})

	},[])

	const [image, setImage] = useState({})
	const [fileName, setFileName] = useState({});

	const fileOnchange = (e) => {
		setImage(e.target.files[0])
	}

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	const handleEditProfile = (data) => {

		let formData = new FormData()

		formData.append("avatar", image);

		fetch(`https://backend-buddyget.herokuapp.com/uploadFile`, {
			method: 'POST',
			body: formData
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setFileName(data)

		})		
		
		const options = {
		  method: 'PUT',
		  headers: {
		    'Authorization': `Bearer ${localStorage.getItem('token')}`,
		    'Content-Type': 'application/json'
		  },
		  body: JSON.stringify({
		  	id: data._id,
		  	firstName: firstName,
		  	lastName: lastName,
		  	mobileNo: mobileNo,
		  	doyName: fileName.fileName
		  })
		}

		fetch(`${AppHelper.API_URL}/users/updateProfile`, options)
		.then(res => res.json())
		.then(data => {
			
			handleClose()

			const options = {
			  headers: {
			    'Authorization': `Bearer ${localStorage.getItem('token')}`,
			    'Content-Type': 'application/json'
			  }
			}

			fetch(`${AppHelper.API_URL}/users/details`, options)
			.then(res => res.json())
			.then(data => {
				setData(data)
			})
		})
	}

	console.log(data);

	const url = [];

	if(data.doyName !== null){
		url.push('https://backend-buddyget.herokuapp.com/static/' + data.doyName)
	}

	
	return(
		<React.Fragment>
			<Container className="bg-warning m-5">
				<Row>
					<Col lg={12}>
						<div className=" m-5 text-center">
							<Image src={url} width={150} fluid className="m-4"/>
							<h3>{data.firstName} {data.lastName}</h3>
							<p>Email: {data.email}</p>
							<p>Phone: {data.mobileNo}</p>
				      		<em>Balance: PHP {data.balance}</em><br/>
				      		<Button variant="primary" onClick={handleShow}>
				      		        Edit Profile
				      		</Button>

						</div>
					</Col>
				</Row>
			</Container>
      		<Modal show={show} onHide={handleClose}>
      		        <Modal.Header closeButton>
      		          <Modal.Title>Modal heading</Modal.Title>
      		        </Modal.Header>
      		        <Modal.Body>
      		        	<Form>
      		        		<input type="file" onChange={fileOnchange}/>
      		        		<Form.Group controlId ="firstName">
      		        			<Form.Label>First Name:</Form.Label>
      		        			<Form.Control type="text" placeholder={data.firstName} value={firstName} onChange={(e) => setFirstName(e.target.value)} required/>
      		        		</Form.Group>

      		        		<Form.Group controlId ="lastName">
      		        			<Form.Label>Last Name:</Form.Label>
      		        			<Form.Control type="text" placeholder={data.lastName} value={lastName} onChange={(e) => setLastName(e.target.value)} required/>
      		        		</Form.Group>

      		        		<Form.Group controlId ="mobileNo">
      		        			<Form.Label>Mobile No:</Form.Label>
      		        			<Form.Control type="number" placeholder={data.mobileNo} value={mobileNo} onChange={(e) => setMobileNo(e.target.value)} required/>
      		        		</Form.Group>
      		        	</Form>
      		        </Modal.Body>
      		        <Modal.Footer>
      		          <Button variant="secondary" onClick={handleClose}>
      		            Close
      		          </Button>
      		          <Button variant="primary" onClick={() => handleEditProfile(data)}>
      		            Save Changes
      		          </Button>
      		        </Modal.Footer>
      		      </Modal>

		</React.Fragment>
	)
}