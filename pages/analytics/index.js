import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Form } from 'react-bootstrap';
import AppHelper from '../../app-helper';
import { Pie } from 'react-chartjs-2';
import {colorRandomizer} from '../../helper'
import { Line } from "react-chartjs-2";
import moment from 'moment';

export default function Analytics(){

	const [startDate, setStartDate] = useState('');
	const [endDate, setEndDate] = useState('');
	const [brands, setBrands] = useState([])
	const [paymentBrands, setPaymentBrands] = useState([])
	const [sales, setSales] = useState([])
	const [paymentSales, setPaymentSales] = useState([])
	const [bgColors, setBgColors] = useState([])
	const [paymentBgColors, setPaymentBgColors] = useState([])
	const [sets, setSets] = useState([])
	const [allDate, setAllDate] = useState([])

	function handleStartDate(e){

		e.preventDefault()

		setStartDate(e.target.value)
	}

	function handleEndDate(e){

		e.preventDefault()

		setEndDate(e.target.value)	
	}

	useEffect(() => {
		const options = {
		  headers: {
		    'Authorization': `Bearer ${localStorage.getItem('token')}`,
		    'Content-Type': 'application/json'
		  }
		}

		fetch(`${AppHelper.API_URL}/transactions/details`, options)
		.then(res => res.json())
		.then(data => {

			console.log(data)
			setSets(data)

			if(data.length > 0){
				setBrands(data.map(element => {
					if(element !== undefined){
						if(element.personId === localStorage.getItem('personId')){
							if(element.transactionType === 'income'){
								return element.categoryName
							}						
						}
					}
				}))
				setSales(data.map(element => {
					if( element !== undefined){
						if(element.personId === localStorage.getItem('personId')){
							if(element.transactionType === 'income'){
								return element.amountTransaction
							}						
						}
					}
				}))
				//randomize the bg color for every brand
				//randomize the bg color for every brand
				setBgColors(data.map(element => {
					if(element !== undefined){
						if(element.personId === localStorage.getItem('personId')){
							if(element.transactionType === 'income'){
								return `#${colorRandomizer()}`
							}						
						}
					}
				}))
			}

			if(data.length > 0){
				setPaymentBrands(data.map(element => {
					if(element !== undefined){
						if(element.personId === localStorage.getItem('personId')){
							if(element.transactionType === 'payment'){
								return element.categoryName
							}						
						}
					}
				}))
				setPaymentSales(data.map(element => {
					if( element !== undefined){
						if(element.personId === localStorage.getItem('personId')){
							if(element.transactionType === 'payment'){
								return element.amountTransaction
							}						
						}
					}
				}))
				//randomize the bg color for every brand
				setPaymentBgColors(data.map(element => {
					if(element !== undefined){
						if(element.personId === localStorage.getItem('personId')){
							if(element.transactionType === 'payment'){
								return `#${colorRandomizer()}`
							}						
						}
					}
				}))
			}
		})
	},[])
	const brands1 = []

	brands.forEach(element => {
			if(element !== undefined){
				brands1.push(element)
			}
		})

	const sales1 = []
	sales.forEach(element => {
			if(element !== undefined){
				sales1.push(element)
			}
		})

	const bgCol = []

	bgColors.forEach(element => {
		if(element !== undefined){
		 bgCol.push(element)
		}
	})

	const data = {
		labels: brands1,
		datasets: [{
			data: sales1,
			backgroundColor: bgCol,
			hoverBackgroundColor: bgCol
		}]
	}

	const pb1 = []
	paymentBrands.forEach(element => {
		if(element !== undefined){
			pb1.push(element)
		}
	})


	const ps = [];

	paymentSales.forEach(element => {
		if(element !== undefined){
			ps.push(element)
		}
	})

	const bgCol2 = [];
	 paymentBgColors.forEach(element => {
		if(element !== undefined){
		 bgCol2.push(element)
		}
	})

	const data2 = {
		labels: pb1,
		datasets: [{
			data: ps,
			backgroundColor: bgCol2,
			hoverBackgroundColor:bgCol2
		}]
	}
		const incomeData = []
		const paymentData = []
		const incomeLabels = []
		const paymentLabels = []
		const incomeBalance = []

		if(sets.length > 0){
			sets.forEach(element => {

				if(moment(startDate).format('MMMM DD YYYY') <= moment(element.dateInput).format('MMMM DD YYYY') && moment(endDate).format('MMMM DD YYYY') >= moment(element.dateInput).format('MMMM DD YYYY')){
					
					if(element.transactionType === 'income' && element.personId === localStorage.getItem('personId')){
						
						incomeData.push(parseInt(element.amountTransaction))
						incomeLabels.push(moment(element.dateInput).format('MMMM DD YYYY'))
				
					}else if(element.transactionType === 'payment' && element.personId === localStorage.getItem('personId')){
						
						paymentData.push(parseInt(element.amountTransaction))
						paymentLabels.push(moment(element.dateInput).format('MMMM DD YYYY'))
						
					}
				}
				
			})	
		}

		const data1 = {
		  labels: incomeLabels,
		  datasets: [
		      {
		      label: "Income Dataset",
		      data: incomeData,
		      fill: true,
		      borderColor: "#742774"
		    }
		  ]
		}

		const options = {
			title: {
				display: true,
				text: 'Line Chart for Income Dataset'
			},
			sclaes:{
				yAxes: [
					{
						ticks:{
							min: 0,
							max: incomeData[incomeData.lenght],
							stepSize: 1000
						}
					}
				]
			}
		}


		const data3 = {
		  labels: paymentLabels,
		  datasets: [
		      {
		      label: "Expense Dataset",
		      data: paymentData,
		      fill: true,
		      borderColor: "green"
		    }
		  ]
		}

		const options3 = {
			title: {
				display: true,
				text: 'Line Chart for Expense Dataset'
			},
			sclaes:{
				yAxes: [
					{
						ticks:{
							min: 0,
							max: paymentData[paymentData.lenght],
							stepSize: 1000
						}
					}
				]
			}
		}

	return(
		<React.Fragment>
			<Container fluid className="cont2 bg-white">
				<Row>
					<Col md={6}>
						<Form.Group controlId="startDate">
			                <Form.Label>Select Start Date</Form.Label>
			                <Form.Control type="date" name="dob" value={startDate} onChange={(e) => handleStartDate(e)} placeholder="Date of Birth" />
			            </Form.Group>
					</Col>
					<Col md={6}>
						<Form.Group controlId="endDate">
			                <Form.Label>Select End Date</Form.Label>
			                <Form.Control type="date" name="dob" value={endDate} onChange={(e) => handleEndDate(e)} placeholder="Date of Birth" />
			            </Form.Group>
					</Col>
				</Row>
				<Row>
					<Col md={6}>
						<h2 className="text-center"> Income Pie Chart </h2>
						<Pie data={data} />
					</Col>
					<Col md={6}>
						<h2 className="text-center"> Expense Pie Chart </h2>
						<Pie data={data2} />
					</Col>
				</Row>
				<Row>
					<Col md={6}>
						<Line data={data1} options={options} />
					</Col>
					<Col md={6}>
						<Line data={data3} options={options3} />
					</Col>
				</Row>
				<Row>
					<Col xs={12} className="text-center">
						<p>Line Chart For Income and Expense Datasets</p>
						<h3>FROM: {moment(startDate).format('MMMM DD YYYY')} <br/> TO: {moment(endDate).format('MMMM DD YYYY')}</h3>
					</Col>
				</Row>
			</Container>
		</React.Fragment>
	)
}