import React from 'react';
import { useContext, useState } from 'react';
import UserContext from '../UserContext';
import Router from 'next/router';
import Login from './login';
import Transaction from '../components/Transaction';
import Profile from '../components/Profile';
import IncomeHistory from '../components/IncomeHistory';
import AppHelper from '../app-helper';
import { Container, Row, Col } from 'react-bootstrap';

export default function Home() {

  const { user } = useContext(UserContext);

  return (
    <React.Fragment>
      { user.id !== null
      	? 
      	<Container fluid className="contHeight">
      		<Row>
      			<Transaction />
      		</Row>
      	</Container>
      	: <Login />
      }
    </React.Fragment>
  )
}
