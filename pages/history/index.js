 import React, { useContext, useEffect, useState } from 'react'
import AppHelper from '../../app-helper';
import UserContext from '../../UserContext';
import { Container, Row, Col, Table, Form, Dropdown, Modal, Button } from 'react-bootstrap';
import moment from 'moment';


export default function History(){
	
	const [transactions, setTransactions] = useState([]);
	const [search, setSearch] = useState('');
	const [transactionType, setTransactionType] = useState('All');
	const [show, setShow] = useState(false);
	const [description, setDescription] = useState('')
	const [amount, setAmount] = useState('')
	const [recordId, setRecordId] = useState('');

	const { user } = useContext(UserContext)

	const [personId, setPersonId] = useState(''); 

	function checkMenu(e){
		setTransactionType(e.target.id)
	}

	useEffect(() => {

		const options = {
		  headers: {
		    'Authorization': `Bearer ${localStorage.getItem('token')}`,
		    'Content-Type': 'application/json'
		  }
		}

		fetch(`${AppHelper.API_URL}/transactions/details`, options)
		.then(res => res.json())
		.then(data => {

		  	if(data.length > 0){
			  setTransactions(
				 	 data.map((element, key) => {
				 	 	setPersonId(element.personId)
				 		if(element.personId === localStorage.getItem('personId')){
				 			return(		
		 						{
		 							_id: element._id,
		 							amountTransaction: element.amountTransaction,
		 							balanceAmount: element.balanceAmount,
		 							categoryName: element.categoryName,
		 							dateInput: element.dateInput,
		 							description: element.description,
		 							personId: element.personId,
		 							transactionType: element.transactionType
		 						}
				 			)
				 		}	
				 	 })
				)	

		  	}
		})		

	},[])
	
	const [items, setItems] = useState('')

	function localHandler(item){
		console.log(item)
		handleShow()
	}

	function deleteHandler(item){
		
		const options ={
			method: 'DELETE',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				recordId: item
			})
		}

		fetch(`${AppHelper.API_URL}/transactions/deleteTransactions`, options)
		.then(res => res.json())
		.then(data => {
			
			const options = {
			  headers: {
			    'Authorization': `Bearer ${localStorage.getItem('token')}`,
			    'Content-Type': 'application/json'
			  }
			}

			fetch(`${AppHelper.API_URL}/transactions/details`, options)
			.then(res => res.json())
			.then(data => {

			  	if(data.length > 0){
				  setTransactions(
					 	 data.map((element, key) => {
					 	 	setPersonId(element.personId)
					 		if(element.personId === localStorage.getItem('personId')){
					 			return(		
			 						{
			 							key: key,
			 							_id: element._id,
			 							amountTransaction: element.amountTransaction,
			 							balanceAmount: element.balanceAmount,
			 							categoryName: element.categoryName,
			 							dateInput: element.dateInput,
			 							description: element.description,
			 							personId: element.personId,
			 							transactionType: element.transactionType
			 						}
					 			)
					 		}	
					 	 })
					)	

			  	}
			})		

		})
		
				

	}

		const handleClose = () =>{
			setAmount('')
			setDescription('')
			setRecordId('')    	  
	        setShow(false)
		}
	    const handleShow = (item) => {
	    	console.log(item)
	    	setItems(item)
	    	setShow(true)
	    };


	let trans = [];

	//console.log(transactions)
	if(transactions.length > 0){
		
		trans = transactions.map((element, key) => {
			if(element === undefined){
				element = ''
			}else{
				if(search === '' && transactionType === element.transactionType){

						return(
							<React.Fragment>
								<tr key={key}>
									<td>{moment(element.dateInput).format('MMMM DD YYYY')}</td>
									<td>{element.description}</td>
									<td>{element.categoryName}</td>
									<td>{element.transactionType}</td>
									<td>{element.amountTransaction}</td>
									<td>
										<a className="text-warning" onClick={() => handleShow(element._id)}>
											edit		
										</a>
										 / 
										<a className="text-warning" onClick={() => deleteHandler(element._id)}>
											delete		
										</a>
									</td>

								</tr>
							</React.Fragment>
						)					

				}else if(search === '' && transactionType === 'All'){

						return(
							<React.Fragment>
								<tr key={key}>
									<td>{moment(element.dateInput).format('MMMM DD YYYY')}</td>
									<td>{element.description}</td>
									<td>{element.categoryName}</td>
									<td>{element.transactionType}</td>
									<td>{element.amountTransaction}</td>
									<td>
										<a className="text-warning" onClick={() => handleShow(element._id)}>
											edit		
										</a>
										 / 
										<a className="text-warning" onClick={() => deleteHandler(element._id)}>
											delete		
										</a>
									</td>

								</tr>
							</React.Fragment>
						)					

				}else if((search === element.description) || (search === element.categoryName) || (search === element.transactionType) || (parseInt(search) === parseInt(element.amountTransaction))){

					return(
						<React.Fragment>
							<tr key={key}>
								<td>{moment(element.dateInput).format('MMMM DD YYYY')}</td>
								<td>{element.description}</td>
								<td>{element.categoryName}</td>
								<td>{element.transactionType}</td>
								<td>{element.amountTransaction}</td>
								<td>
									<a className="text-warning" onClick={() => handleShow(element._id)}>
										edit		
									</a>
									 / 
									<a className="text-warning" onClick={() => deleteHandler(element._id)}>
										delete		
									</a>
								</td>

							</tr>
						</React.Fragment>
					)
				}
			}
		})
		
	}

	const [catName, setCatName] = useState('')
    const [dateInput, setDateInput] = useState(new Date)
    	
    function anotherLocalHandler(e){
    	e.preventDefault()

    	const options = {
    		method: 'PUT',
    		headers: {
    			'Authorization': `Bearer ${localStorage.getItem('token')}`,
    			'Content-Type': 'application/json'
    		},
    		body: JSON.stringify({
    			_id: recordId,
    			personId: personId,
    			transactionType: transactionType,
    			categoryName: catName,
    			description: description,
    			amountTransaction: amount,
    			dateInput: dateInput
    		})   		
    	}

    	fetch(`${AppHelper.API_URL}/transactions/editTransactions`, options)
    	.then(res => res.json())
    	.then(data => {
    		setShow(false)
    		const options1 = {
    		  headers: {
    		    'Authorization': `Bearer ${localStorage.getItem('token')}`,
    		    'Content-Type': 'application/json'
    		  }
    		}

    		fetch(`${AppHelper.API_URL}/transactions/details`, options1)
    		.then(res => res.json())
    		.then(data => {
    		  	if(data.length > 0){
    			  setTransactions(
    				 	 data.map((element, key) => {
    				 	 	setPersonId(element.personId)
    				 		if(element.personId === localStorage.getItem('personId')){
    				 			return(		
    		 						{
    		 							key: key,
    		 							_id: element._id,
    		 							amountTransaction: element.amountTransaction,
    		 							balanceAmount: element.balanceAmount,
    		 							categoryName: element.categoryName,
    		 							dateInput: element.dateInput,
    		 							description: element.description,
    		 							personId: element.personId,
    		 							transactionType: element.transactionType
    		 						}
    				 			)
    				 		}	
    				 	 })
    				)	

    		  	}
    		})
    	})
    }

    function checkMenu(e){
    	setTransactionType(e.target.id)
    }

    

    let editModal = transactions.map((element, key) => {
    					if(element === undefined){
							element = ''
						}else{

							if(element._id === items){
								
								return(
									<React.Fragment>
										<p key={key} value={dateInput} onSubmit={(e) => setDateInput(element.dateInput)}>Date: {moment(element.dateInput).format('MMMM DD YYYY')}</p>
										<p value={personId} onSubmit={(e) => setPersonId(element.personId)}></p>
										
										<Form.Group controlId ="recordId">
											<Form.Label>Record ID: </Form.Label>
											<Form.Control type="text"  value={recordId} onChange={(e) => setRecordId(element._id)} required/>
										</Form.Group>

										<Form.Group controlId ="description">
											<Form.Label>Description: </Form.Label>
											<Form.Control  type="text" placeholder={element.description} value={description} onChange={(e) => setDescription(e.target.value)} required/>
										</Form.Group>

										<Form.Group controlId ="categoryName">
											<Form.Label>Category: </Form.Label>
											<Form.Control  type="text" placeholder={element.categoryName} value={catName} onChange={(e) => setCatName(e.target.value)} required/>
										</Form.Group>

										<Form.Group controlId ="Amount">
											<Form.Label>Amount: </Form.Label>
											<Form.Control  type="text" placeholder={element.amountTransaction} value={amount} onChange={(e) => setAmount(e.target.value)} required/>
										</Form.Group>

										<Dropdown className="dropdownMenus" onClick={(e) => checkMenu(e)}>
										  <Dropdown.Toggle variant="secondary btn-block">
										    {transactionType ? transactionType : 'Transaction Type'}
										  </Dropdown.Toggle>

										  <Dropdown.Menu>
										    <Dropdown.Item id="income">Income</Dropdown.Item>
										    <Dropdown.Item id="payment">Payment</Dropdown.Item>
										  </Dropdown.Menu>
										</Dropdown>
									</React.Fragment>
								)
							}
						}

					})

	return(
		<React.Fragment>
			<Container fluid className="bg-primary contH">
				<Row>
					<Col md={6}>
						<Form>
							<Form.Group className="text-transact m-3" controlId ="search">
								<Form.Control  type="text" placeholder="Search here..." value={search} onChange={e => setSearch(e.target.value)} required/>
							</Form.Group>
						</Form>
					</Col>
					<Col md={6}>
						<Form>
							<Dropdown className="dropdownMenus" onClick={(e) => checkMenu(e)}>
							  <Dropdown.Toggle variant="secondary btn-block">
							    {transactionType ? transactionType : 'All'}
							  </Dropdown.Toggle>

							  <Dropdown.Menu>
							    <Dropdown.Item id="income">Income</Dropdown.Item>
							    <Dropdown.Item id="payment">Payment</Dropdown.Item>
							    <Dropdown.Item id="All">All</Dropdown.Item>
							  </Dropdown.Menu>
							</Dropdown>
						</Form>
					</Col>
				</Row>
				<Row>
					<Col>
					</Col>
					<Col lg={8}>
						<div className="tblHistory">
							<Table striped bordered hover className="text-white">
								<thead className="bg-white text-dark">
								    <tr>
								      <th>Date</th>
								      <th>Description</th>
								      <th>Category</th>
								      <th>Type</th>
								      <th>Amount (PHP)</th>
								      <th>Edit/Delete</th>
								    </tr>
								</thead>
								<tbody>
									{trans.reverse()}
								</tbody>
							</Table>
						</div>
					</Col>
					<Col>
					</Col>
				</Row>
			</Container>

			<Modal show={show} onHide={handleClose}>
			        <Modal.Header closeButton>
			          <Modal.Title>Modal heading</Modal.Title>
			        </Modal.Header>
			        <Modal.Body>
			        	<Form onSubmit = {(e) => anotherLocalHandler(e)}>
			        		{items
			        			?
			        				editModal
			        			:
			        				'No items'
			        		}
			        		<Button variant="secondary" onClick={handleClose}>
			        		  Close
			        		</Button>
			        		<Button type="submit" variant="primary">
			        		  Save Changes
			        		</Button>
			       		</Form>
			       	</Modal.Body>
			      </Modal>

		</React.Fragment>
	)
}