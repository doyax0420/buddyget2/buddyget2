import React, { useState, useEffect, useContext } from 'react';
import AppHelper from '../../app-helper'

export default function editRecord(){
	//console.log(transactions)

	const [transactions, setTransactions] = useState([])

	useEffect(() => {
		console.log(localStorage.getItem('recordId'))

		const options = {
		  headers: {
		    'Authorization': `Bearer ${localStorage.getItem('token')}`,
		    'Content-Type': 'application/json'
		  }
		}

		fetch(`${AppHelper.API_URL}/users/details`, options)
		.then(res => res.json())
		.then(data => {

		  console.log(data)

		  let trans = data.transactions.map((element, key) => {
		   
		   		if(element._id === localStorage.getItem('recordId')){
		   			return(
		   				<React.Fragment>
		   					<p key={key}>{element._id}</p>
		   					<p>{element.categoryName}</p>
		   				</React.Fragment>
		   				)
		   		}
		   	
		  })

		  setTransactions(trans)
		})		

	}, [])

	console.log(transactions)
    return(
      <React.Fragment>
      	<h1>{transactions}</h1>
      </React.Fragment>
    )
}

// export async function getStaticPaths(){

//  const rest = await fetch(`${AppHelper.API_URL}users/details`,{
//   	method: 'GET',
//   	headers: {
//   		'Authorization': `Bearer ${localStorage.getItem('token')}`,
//   		'Content-Type': 'application/json'
//   	}
//   })
  
//   const data  = await rest.json()

//   const paths = data.transactions.map(element => ({

// 	      params: { id: element._id }

// 	  }))

// 	return { paths, fallback: false }

// }

// export async function getStaticProps({ params }){

//   const rest = await fetch(`${AppHelper.API_URL}users/details`,{
//     	method: 'GET',
//     	headers: {
//     		'Authorization': `Bearer ${localStorage.getItem('token')}`,
//     		'Content-Type': 'application/json'
//     	}
//     })
    
//     const data  = await rest.json()

//   const transactions = data.transactions.find(element => element._id === params.id)

//   return {
//   	props: {
//   		transactions
//   	}
//   }
// }