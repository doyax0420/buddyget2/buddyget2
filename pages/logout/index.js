import{ useEffect, useContext } from 'react';
import UserContext from '../../UserContext';
import Router from 'next/router';

export default function index(){
	//Consume UserContext and obtain unsetUser and setUser from it
	const { unsetUser, setUser } = useContext(UserContext)
	//Invoke unsetUser()
	
	useEffect(() => {
		unsetUser();
		Router.push('/login')
	}, [])

	return null
}